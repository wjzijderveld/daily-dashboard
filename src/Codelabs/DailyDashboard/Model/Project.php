<?php
/**
 * This file and its content is copyright of Beeldspraak Website Creators BV - (c) Beeldspraak 2012. All rights reserved.
 * Any redistribution or reproduction of part or all of the contents in any form is prohibited.
 * You may not, except with our express written permission, distribute or commercially exploit the content.
 *
 * @author      Beeldspraak <info@beeldspraak.com>
 * @copyright   Copyright 2012, Beeldspraak Website Creators BV
 * @link        http://beeldspraak.com
 *
 */

namespace Codelabs\DailyDashboard\Model;


class Project
{
    private $id;

    private $ent;

    private $opt;

    private $name;

    private $created;

    private $lastUsed;

    public function __construct()
    {
        $this->created = new \DateTime('now');
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $ent
     */
    public function setEnt($ent)
    {
        $this->ent = $ent;
    }

    /**
     * @return mixed
     */
    public function getEnt()
    {
        return $this->ent;
    }

    /**
     * @param mixed $opt
     */
    public function setOpt($opt)
    {
        $this->opt = $opt;
    }

    /**
     * @return mixed
     */
    public function getOpt()
    {
        return $this->opt;
    }

    /**
     * @param mixed $lastUsed
     */
    public function setLastUsed($lastUsed)
    {
        $this->lastUsed = $lastUsed;
    }

    /**
     * @return mixed
     */
    public function getLastUsed()
    {
        return $this->lastUsed;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

}