<?php
namespace Codelabs\DailyDashboard\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;

class ProjectManager {
    
    private $connection;

    private $timeOffset;
    
    public function __construct(Connection $connection, $timeOffset = 0)
    {
        $this->connection = $connection;
        $this->timeOffset = $timeOffset;
    }

    /**
     * @return ArrayCollection
     */
    public function getProjects()
    {
        $projects = new ArrayCollection();
        $queryBuilder = $this->connection->createQueryBuilder();

        $queryBuilder
            ->from($this->getTable('project'), 'p')
            ->select(array(
                'p.Z_PK',
                'p.Z_ENT',
                'p.Z_OPT',
                sprintf('datetime("2001-01-01", (ZCREATED + %d) || " seconds") AS ZCREATED', $this->timeOffset),
                sprintf('datetime("2001-01-01", (ZLASTUSED + %d) || " seconds") AS ZLASTUSED', $this->timeOffset),
                'p.ZNAME',
            ))
        ;

        // echo $queryBuilder->getSQL(); exit;

        foreach ($queryBuilder->execute() as $result) {
            $projects->set($result['Z_PK'], $this->hydrateProject($result));
        }

        return $projects;
    }

    public function findProject($id)
    {
        $queryBuilder = $this->connection->createQueryBuilder();

        $queryBuilder
            ->from($this->getTable('project'), 'p')
            ->select(array(
                'p.Z_PK',
                'p.Z_ENT',
                'p.Z_OPT',
                sprintf('datetime("2001-01-01", (ZCREATED + %d) || " seconds") AS ZCREATED', $this->timeOffset),
                sprintf('datetime("2001-01-01", (ZLASTUSED + %d) || " seconds") AS ZLASTUSED', $this->timeOffset),
                'p.ZNAME',
            ))
            ->where('p.Z_PK = :id')
            ->setParameter('id', $id)
        ;

        $result = $queryBuilder->execute()->fetch();
        if (empty($result)) {
            return null;
        }

        return $this->hydrateProject($result);
    }

    public function getSessions()
    {
        $sessions = new ArrayCollection();
        $queryBuilder = $this->connection->createQueryBuilder();

        $queryBuilder
            ->from($this->getTable('session'), 's')
            ->select('s.*')
        ;

        foreach ($queryBuilder->execute() as $data) {
            $sessions->add($data);
        }

        return $sessions;
    }

    /**
     * @param Project $project
     * @return ArrayCollection
     */
    public function getSessionsForProject(Project $project, \DateTime $dateTimeFrom = null, \DateTime $dateTimeTill = null)
    {

        var_dump($project->getName(), $this->getSamplesForProject($project, $dateTimeFrom, $dateTimeTill));
        exit;

        $sessions = new ArrayCollection();
        $queryBuilder = $this->connection->createQueryBuilder();

        $queryBuilder
            ->from($this->getTable('session'), 's')
            ->select(array(
                's.Z_PK',
                's.Z_ENT AS session_ent',
                's.Z_OPT AS session_opt',
                's2.Z_ENT AS sample_ent',
                's2.Z_OPT AS sample_opt',
                's2.ZSESSIONREPRESENTATIVE',
                's2.ZSESSION',
                's2.ZPROJECT',
                sprintf('datetime("2001-01-01", (s2.ZCREATED + %d) || " seconds") AS SAMPLE_CREATED_DATE', $this->timeOffset),
                sprintf('datetime("2001-01-01", (s.ZSTARTED + %d) || " seconds") AS ZSTARTED_DATE', $this->timeOffset),
                sprintf('datetime("2001-01-01", (s.ZENDED + %d) || " seconds") AS ZENDED_DATE', $this->timeOffset),
            ))
            ->join('s', $this->getTable('sample'), 's2', 's2.ZSESSION = s.Z_PK AND s2.ZPROJECT = :projectId')
            ->setParameter('projectId', $project->getId())
            ->groupBy('s.Z_PK')
        ;

        if (null !== $dateTimeFrom) {
            $queryBuilder->andWhere('s.ZSTARTED > :since')->setParameter('since', $this->dateTimeToTimestampSince2001($dateTimeFrom));
        }

//         echo $queryBuilder->getSQL() , '<br />' . $this->dateTimeToTimestampSince2001($dateTimeFrom);exit;

        var_dump($project->getId() . ': ' .  $project->getName());
        foreach ($queryBuilder->execute() as $session) {
            var_dump($session);
            $sessions->add($session);
        }


        return $sessions;
    }

    /**
     * @param Project $project
     * @param \DateTime $dateTimeFrom
     * @param \DateTime $dateTimeTill
     * @return \DateInterval|null
     */
    public function getAggregatedSamplesForProject(Project $project, \DateTime $dateTimeFrom, \DateTime $dateTimeTill)
    {
        $samples = new ArrayCollection();

        $queryBuilder = $this->connection->createQueryBuilder();

        $queryBuilder
            ->from($this->getTable('sample'), 's')
            ->select(array(
                's.Z_PK',
                's.ZPROJECT',
                's.ZSESSION',
                's.ZSESSIONREPRESENTATIVE',
                sprintf('datetime("2001-01-01", (s2.ZSTARTED + %d) || " seconds") AS SESSION_STARTED_DATE', $this->timeOffset),
                sprintf('datetime("2001-01-01", (s2.ZENDED + %d) || " seconds") AS SESSION_ENDED_DATE', $this->timeOffset),
                sprintf('datetime("2001-01-01", (s.ZCREATED + %d) || " seconds") AS CREATED_DATE', $this->timeOffset),
            ))
            ->join('s', 'ZSESSION', 's2', 's.ZSESSION = s2.Z_PK')
            ->groupBy('s2.Z_PK, s.Z_PK')
        ;

        if (null !== $dateTimeFrom) {
            $queryBuilder->andWhere('s.ZCREATED > :since')->setParameter('since', $this->dateTimeToTimestampSince2001($dateTimeFrom));
        }

        if (null !== $dateTimeTill) {
            $queryBuilder->andWhere('s.ZCREATED < :till')->setParameter('till', $this->dateTimeToTimestampSince2001($dateTimeTill));
        }

        $queryBuilder->orderBy('s.ZCREATED');

//        echo $queryBuilder->getSQL() . '<br />';
//        echo var_export($queryBuilder->getParameters(), true);

        $timestamp = $previousTimestamp = $previousProject = null;
        $projectDiffs = array();
        foreach ($queryBuilder->execute() as $sample) {
            if ($previousTimestamp instanceof \DateTime) {

                if ($project->getId() == 66) {
//                    var_dump($sample['ZPROJECT'], $sample['ZSESSIONREPRESENTATIVE'], $sample['CREATED_DATE'], $sample['SESSION_STARTED_DATE'], $sample['SESSION_ENDED_DATE'], '----');
                }
                $timestamp = new \DateTime($sample['CREATED_DATE']);
                if ($project->getId() == 66) {
//                    var_dump($previousTimestamp->diff($timestamp, true));
                }
                $projectDiffs[$previousProject][] = $previousTimestamp->diff($timestamp, true);

            }

            $previousProject = $sample['ZPROJECT'];
            if ($timestamp instanceof \DateTime && $previousTimestamp instanceof \DateTime && $timestamp->format('d') !== $previousTimestamp->format('d')) {
                $previousTimestamp = null;
            } else {
                $previousTimestamp = new \DateTime($sample['CREATED_DATE']);
            }
        }

        foreach ($projectDiffs as $projectId => $diffs) {
            $end = new \DateTime('now');
            foreach ($diffs as $diff) {
                $end->add($diff);
            }
            $now = new \DateTime('now');
            $samples->set($projectId, $now->diff($end));
        }

        return $samples->get($project->getId());
    }

    /**
     * Returns all samples for a project in a specified time period
     *
     * @param Project $project
     * @param \DateTime $dateTimeFrom
     * @return ArrayCollection
     */
    public function getSamplesForProject(Project $project, \DateTime $dateTimeFrom = null, \DateTime $dateTimeTill = null)
    {
        $samples = new ArrayCollection();
        $queryBuilder = $this->connection->createQueryBuilder();

        $queryBuilder
            ->from($this->getTable('sample'), 's')
            ->select(array(
                's.Z_PK',
                's.ZSESSION',
                sprintf('datetime("2001-01-01", (s.ZCREATED + %d) || " seconds") AS CREATED_DATE', $this->timeOffset),

            ))
        ;

        if (null !== $dateTimeFrom) {
            $queryBuilder->andWhere('s.ZCREATED > :since')->setParameter('since', $this->dateTimeToTimestampSince2001($dateTimeFrom));
        }

        if (null !== $dateTimeTill) {
            $queryBuilder->andWhere('s.ZCREATED < :till')->setParameter('till', $this->dateTimeToTimestampSince2001($dateTimeTill));
        }

        foreach ($queryBuilder->execute() as $sample) {
            $samples->add($sample);
        }

        return $samples;
    }

    public function getSamples()
    {
        $samples = new ArrayCollection();
        $queryBuilder = $this->connection->createQueryBuilder();

        $queryBuilder
            ->from($this->getTable('sample'), 's')
            ->select(array(
                's.Z_PK',
                's.Z_ENT',
                's.Z_OPT',
                's.ZPROJECT',
                's.ZSESSION',
                sprintf('datetime("2001-01-01", (s.ZCREATED + %d) || " seconds") AS ZCREATED', $this->timeOffset),
            ))
        ;

        foreach ($queryBuilder->execute() as $data) {
            $samples->add($data);
        }

        return $samples;
    }

    /**
     * @param array $data
     * @param Project $project
     * @return Project
     */
    public function hydrateProject(array $data, Project $project = null)
    {
        if (null === $project) {
            $project = new Project();
        }

        if (isset($data['Z_PK'])) {
            $project->setId($data['Z_PK']);
        }

        if (isset($data['Z_ENT'])) {
            $project->setEnt($data['Z_ENT']);
        }

        if (isset($data['Z_OPT'])) {
            $project->setOpt($data['Z_OPT']);
        }

        if (isset($data['ZNAME'])) {
            $project->setName($data['ZNAME']);
        }

        if (isset($data['ZCREATED'])) {
            $project->setCreated(new \DateTime($data['ZCREATED']));
        }

        if (isset($data['ZLASTUSED'])) {
            $project->setLastUsed(new \DateTime($data['ZLASTUSED']));
        }

        return $project;
    }

    /**
     * @param $table string
     * @return string
     * @throws \InvalidArgumentException
     */
    public function getTable($table)
    {
        switch ($table) {
            case 'project':
                return 'ZPROJECT';
            case 'session':
                return 'ZSESSION';
            case 'sample':
                return 'ZSAMPLE';
            case 'metadata':
                return 'Z_METADATA';
            case 'primarykey':
                return 'Z_METADATA';
            default:
                throw new \InvalidArgumentException(sprintf('Unknown table "%s"', $table));
        }
    }

    /**
     * @return \Doctrine\DBAL\Schema\Table[]
     */
    public function getSchema()
    {
        $tables = $this->connection->getSchemaManager()->listTables();

        return $tables;
    }

    /**
     * @param \DateTime $dateTime
     * @return int
     */
    public function dateTimeToTimestampSince2001(\DateTime $dateTime)
    {
        $zeroPoint = (new \DateTime('2001-01-01'))->getTimestamp();

        return $dateTime->getTimestamp() - $zeroPoint - $this->timeOffset;
    }
}

?>