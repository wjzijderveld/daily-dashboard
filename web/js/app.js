/**
 * Created by willemjan on 10/18/13.
 */

(function($) {
    "use strict";

    var App = new function () {
        var dateFrom, dateTill;
        this.totalSelectedTime = 0;

        this.formatTime = function(time) {
            var duration, hours, minutes, result = '';
            duration = moment.duration(time, 'seconds');
            hours = duration.hours();
            minutes = duration.minutes();
            minutes += (duration.seconds() >= 30 ? 1 : 0); // Add a minute when we have more then 30 seconds

            if (hours > 0) {
                result = hours + 'u ';
            }

            if (minutes > 0) {
                result += minutes + 'm ';
            }

            return result;
        };

        this.calculateTimes = function (event) {
            var item = $(event.currentTarget),
                seconds = parseInt(item.data('seconds'), 10);

            if (item.hasClass('active')) {
                this.totalSelectedTime -= seconds;
                item.removeClass('active');
            } else {
                this.totalSelectedTime += seconds;
                item.addClass('active');
            }

            $('#calculatedTime').text(this.formatTime(this.totalSelectedTime));
        };

        $('.project-row').click($.proxy(this.calculateTimes, this));

        dateFrom = $('#datefrom');
        dateTill = $('#datetill');

        dateFrom.datetimepicker();
        dateTill.datetimepicker();

        var date;
        if (dateFrom.data('default')) {
            date = new Date(dateFrom.data('default'));
            dateFrom.datetimepicker('setValue', date.getTime() + (-1 * date.getTimezoneOffset() * 60000));
        }

        if (dateTill.data('default')) {
            date = new Date(dateTill.data('default'));
            dateTill.datetimepicker('setValue', date.getTime() + (-1 * date.getTimezoneOffset() * 60000));
        }
    }

}(jQuery));
