<?php
/**
 * This file and its content is copyright of Beeldspraak Website Creators BV - (c) Beeldspraak 2012. All rights reserved.
 * Any redistribution or reproduction of part or all of the contents in any form is prohibited.
 * You may not, except with our express written permission, distribute or commercially exploit the content.
 *
 * @author      Beeldspraak <info@beeldspraak.com>
 * @copyright   Copyright 2012, Beeldspraak Website Creators BV
 * @link        http://beeldspraak.com
 *
 */

use Codelabs\DailyDashboard\Model\ProjectManager;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

require 'vendor/autoload.php';

ErrorHandler::register();
ExceptionHandler::register();

$configFile = __DIR__ . '/parameters.yml';
if (!file_exists($configFile)) {
    throw new RuntimeException('You should create a parameters.yml, look at parameters.yml.dist');
}

$config = Yaml::parse(file_get_contents($configFile), true);

$values = array(
    'dd.db_path'    => $config['parameters']['daily.sqlite_path'],
);

$app = new Silex\Application($values);
$app->register(new \Silex\Provider\TwigServiceProvider());
$app->register(new \Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'    => 'pdo_sqlite',
        'path'      => $app['dd.db_path']
    ),
));

$date = new \DateTime('now');
$app['dd.time_offset'] = $date->getOffset();

$app['twig.path'] = array(
    __DIR__ . '/templates'
);

$app->get('/', function () use ($app) {

    /** @var \Twig_Environment $twig */
    $twig = $app['twig'];
    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];
    $showAll = (bool)$request->query->get('showAll', 0);

    $manager = new ProjectManager($app['db'], $app['dd.time_offset']);
    $projects = $manager->getProjects();

    $dateFrom = (new \DateTime($request->query->get('datefrom', 'now')))->setTime(0, 0, 0);
    $dateTill = (new \DateTime($request->query->get('datetill', 'now')))->setTime(23, 59, 59);


    $projectTimes = array();
    $total = new \DateTime('now');
    $zero = time();
    foreach ($projects as $project) {
        $diff = $manager->getAggregatedSamplesForProject($project, $dateFrom, $dateTill);
        if ($diff) {
            $datetime = new \DateTime('now');
            $datetime->add($diff);
            $projectTimes[$project->getId()] = array('diff' => $diff, 'seconds' => $datetime->getTimestamp() - $zero);

            $total->add($diff);
        }
    }
    $totalTime = $total->getTimestamp() - $zero;

    $twig->addFunction($app['time_calculator']);
    return $twig->render('dashboard.html.twig', array(
        'projects'      => $projects,
        'projectTimes'  => $projectTimes,
        'totalTime'     => $totalTime,
        'dateFrom'      => $dateFrom,
        'dateTill'      => $dateTill,
    ));
});

$app['time_calculator'] = $app->share(function($app) {

    // TODO: refactor this to an object
    // $calculator->fromSeconds
    return new Twig_SimpleFunction('timeCalculator', function($time, $to = '%i:%s', $from = 'seconds') use($app) {

        $years = $months = $weeks = $days = $hours = $minutes = $seconds = null;

        switch ($from) {
            case 'seconds':

                $seconds = $time;
                $minutes = $time / 60;
                $hours = $minutes / 60;
//                $days = $hours / 8;

                break;
            default:
                throw new \InvalidArgumentException(sprintf('Unsupported format "%s" given', $from));
        }

        if ($minutes > 1) {
            $seconds %= 60;
        }

        if ($hours > 1) {
            $minutes %= 60;
        }

        if ($days > 1) {
            $hours %= 24;
        }

        return strtr($to, array(
            '%D' => sprintf('%02d', $days),
            '%H' => sprintf('%02d', $hours),
            '%h' => sprintf('%02d', floor($hours / 2)),
            '%G' => sprintf('%d', $hours),
            '%g' => sprintf('%d', floor($hours / 2)),
            '%i' => sprintf('%02d', $minutes),
            '%s' => sprintf('%02d', $seconds),
        ));
    });

});

$app->get('/schema', function () use ($app) {

    if (!$app['debug']) {
        throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }

    /** @var \Twig_Environment $twig */
    $twig = $app['twig'];

    $manager = new ProjectManager($app['db'], $app['dd.time_offset']);

    return $twig->render('schema.html.twig', array(
        'schema' => $manager->getSchema(),
    ));

});